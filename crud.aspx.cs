﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication1
{
    public partial class crud : System.Web.UI.Page
    {
        koneksiSQL koneksi = new koneksiSQL("localhost", "webscience", "root");
        public MySqlDataReader results;
        string edNpm;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.results = koneksi.openQuerySQL("SELECT npm,nama,kelas,ttl,alamat FROM users;");

            //this.results = koneksi.querySQL("SELECT npm,nama,kelas,ttl,alamat FROM usersemptytesting;");
        }

        protected void BuatTabel()
        {
            dataSql.DataSource = this.results;
            dataSql.DataBind();
            this.results.Close();
            this.results = null;
            this.koneksi.closeQuerySQL();
        }

        protected void SaveData(object sender, EventArgs e)
        {
            koneksi.closeQuerySQL();
            koneksi.openQuerySQL(String.Format(
                            "UPDATE users SET npm=\"{0}\",nama=\"{1}\",kelas=\"{2}\",ttl=\"{3}\",alamat=\"{4}\" where npm=\"{5}\"",
                            npm.Value, nama.Value, kelas.Value, ttl.Value, alamat.Value, Request.Cookies["target"].Value
                            ));
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }

        protected void AddData(object sender, EventArgs e)
        {
            koneksi.closeQuerySQL();
            koneksi.openQuerySQL(String.Format(
                            "INSERT INTO users(npm,nama,kelas,ttl,alamat) VALUES(\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\");",
                            npm.Value, nama.Value, kelas.Value, ttl.Value, alamat.Value
                            ));
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }

        protected void DelData(object sender, EventArgs e)
        {
            koneksi.closeQuerySQL();
            koneksi.openQuerySQL(String.Format("DELETE FROM users WHERE npm={0};", Request.Cookies["target"].Value));
            Response.Redirect(HttpContext.Current.Request.Url.ToString(), true);
        }
    }
}