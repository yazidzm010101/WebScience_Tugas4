﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql;
using MySql.Data.MySqlClient;


namespace WebApplication1
{
    public class koneksiSQL
    //aim : create sql connection, run a query,
    {
        MySqlConnection koneksi;

        public koneksiSQL(string serverF,string database,string userF)
        {
            String queryF = String.Format("server={0};user id={1};database={2};", serverF, userF,database);
            MySqlConnection koneksiF = new MySqlConnection(queryF);
            this.koneksi = koneksiF;
        }

        public koneksiSQL(string serverF,string userF,string passF,string database)
        {
            String queryF = String.Format("server={0};user id={1};password={2};database={3};", serverF, userF,passF,database);
            MySqlConnection koneksiF = new MySqlConnection(queryF);
            this.koneksi = koneksiF;
        }


        public MySqlDataReader openQuerySQL(string queryF)
        {
            //Membuat SQLCommand menggunakan Query dan Koneksi yang telah ditentukan
            MySqlCommand comm = new MySqlCommand(queryF, this.koneksi);

            //Menghubungkan Koneksi tersebut
            this.koneksi.Open();

            //Mengexecute SQLCommand dan me-Retrieve data 
            MySqlDataReader resultsF = comm.ExecuteReader();

            //Jika Ada Data(Ketemu)}

            return resultsF;
        }

        public void closeQuerySQL()
        {
            this.koneksi.Close();
        }
    }
}