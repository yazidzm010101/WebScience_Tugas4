﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace WebApplication1
{
    public partial class dashboard : System.Web.UI.Page
    {
        public MySqlDataReader results;
        koneksiSQL koneksi = new koneksiSQL("localhost", "webscience", "root");
        String userIn;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.userIn = Request.Cookies["sessionUsr"].Value;
            AmbilData();
        }

        protected void AmbilData()
        {
            String querySQL = String.Format("SELECT npm,nama,kelas,ttl,alamat FROM users WHERE npm='{0}';",userIn); 
            this.results = koneksi.openQuerySQL(querySQL);
        }

       protected bool GenerateKonten()//Buat Generate Konten
        {
            bool execute;
            if (this.results.HasRows)
            {
                while (this.results.Read())
                {
                    Welcome.InnerText = "Selamat datang kembali, " + results[1].ToString();
                    NPM.InnerText = results[0].ToString();
                    namaTd.InnerText = results[1].ToString();
                    kelasTd.InnerText = results[2].ToString();
                    ttlTd.InnerText = results[3].ToString();
                    alamatTd.InnerText = results[4].ToString();
                }
                execute = true;
            }
            else
            {
                execute = false;
            }
            this.results.Close();
            this.results = null;
            this.koneksi.closeQuerySQL();
            return execute;
        }

        protected void btnBuka_Click(object sender, EventArgs e)
        {
            Response.Redirect("crud.aspx");
        }
    }
}