﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebApplication1.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Selamat Datang</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/_font.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/myForm.css"/>
</head>
<body>
    <div class="Header">
        <div class="Home">
        <a href="index.aspx">
            <img src="assets/img/teamL.png" alt="Logo"/>
            <label class="Logo">Loginin Gan</label>
        </a>
        </div>
        <div class="HeaderLink">
            <a href="index.aspx">Masuk</a>
        </div>
    </div>
    <div class="Kontainer" runat="server">
            <form class="MyForm" name="MyForm" runat="server">
                <div class="Title">
                        <img src="assets/img/team.png" alt="Logo"/>
                        <label class="Logo">Loginin Gan</label>
                </div>
                <label class="MyLabel">NPM</label>
                <input class="MyInput" type="text" id="username" runat="server"/>
                <label class="MyLabel">Kata Sandi</label>
                <input class="MyInput" type="password" id="password" runat="server"/>
                <br/>
                <asp:button id="Submit" class="MySubmit" onclick="Validasi" runat="server" Text="Masuk"/>
                <div class="modal fade" tabindex="-1" id="myModal" role="dialog" aria-modal="false" onclick="window.open('index.aspx','_self')">
                    <div class="modal-dialog" role="document">
                        <asp:ScriptManager ID="ScriptManager1" runat="server" />
                        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h5>
                                    </div>
                                    <div class="modal-body">
                                        <p><asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label></p>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </form>
    </div>
    <div class="KontainerBg"></div>
    <div class="Footer">
        <img src="assets/img/teamL.png" alt="Team"/>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        <img src="assets/img/user.png" alt="User"/>
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
</body>
</html>
