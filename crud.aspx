﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="crud.aspx.cs" Inherits="WebApplication1.crud" %>
<%@Import namespace="MySql.Data.MySqlClient"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Database</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link href="assets/css/_font.css" rel="stylesheet" />
    <link href="assets/css/master.css" rel="stylesheet" />
    <link href="assets/css/myForm.css" rel="stylesheet" />
    <link href="assets/css/crud.css" rel="stylesheet" />
</head>
<body>
    </div>
    <div class="Header">
        <div class="Home">
        <a href="dashboard.aspx">
                <img src="assets/img/teamL.png" alt="Logo"/>
            <label class="Logo">Loginin Gan</label>
        </a>
        </div>
        <div class="HeaderLink">
            <a href="index.aspx">Keluar</a>
        </div>
    </div>
    <div class="Konten">
    <div class="Kontainer">
        <div id="tabel">
            <%if (this.results.HasRows)
                {
                    BuatTabel();%>
                    <asp:Repeater ID="dataSql" runat="server">
                        <HeaderTemplate>
                            <br />
                            <br />
                            <h2 class="text-center">Menampilkan Database</h2>
                            <br />
                            <table class="table table-striped">
                                    <tr class="thead-dark">
                                        <th>No.</th>
                                        <th>NPM</th>
                                        <th>Nama</th>
                                        <th>Kelas</th>
                                        <th>Tempat,Tanggal Lahir</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <th scope="row"><%#Container.ItemIndex + 1%></th>
                                <td class="r<%#Container.ItemIndex + 1%>"><%#DataBinder.Eval(Container.DataItem,"npm")%></td>
                                <td class="r<%#Container.ItemIndex + 1%>"><%#DataBinder.Eval(Container.DataItem,"nama")%></td>
                                <td class="r<%#Container.ItemIndex + 1%>"><%#DataBinder.Eval(Container.DataItem,"kelas")%></td>
                                <td class="r<%#Container.ItemIndex + 1%>"><%#DataBinder.Eval(Container.DataItem,"ttl")%></td>
                                <td class="r<%#Container.ItemIndex + 1%>"><%#DataBinder.Eval(Container.DataItem,"alamat")%></td>
                                <td>
                                    <button id="r<%#Container.ItemIndex + 1%>" type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal" onclick="getRow(this.id)">Ubah</button>
                                    <button id="r<%#Container.ItemIndex + 1%>Del" type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" onclick="delRow(this.id)">Hapus</button>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
            <%}%>
            </div>
                <br />
                <center><button id="rAdd" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="addRow()">Tambah</button></center>
            <form runat="server">
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="judul"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                          <p id="formMsg"></p>
                          <div id="formEdit">
                              <div class="form-group">
                                  <label>NPM</label>
                                  <input class="form-control" type="text" id="npm" runat="server"/>
                              </div>
                              <div class="form-group">
                                  <label>Nama</label>
                                  <input class="form-control" type="text" id="nama" runat="server"/>
                              </div>
                              <div class="form-group">
                                  <label>Kelas</label>
                                  <input class="form-control" type="text" id="kelas" runat="server"/>
                              </div>
                              <div class="form-group">
                                  <label>Tempat, Tanggal Lahir</label>
                                  <input class="form-control" type="text" id="ttl" runat="server"/>
                              </div>
                              <div class="form-group">
                                  <label>Alamat</label>
                                  <input class="form-control" type="text" id="alamat" runat="server"/>
                              </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <asp:button runat="server" type="button" id="saveRow" class="btn btn-success" onclick="SaveData" Text="Simpan perubahan"/>
                        <asp:button runat="server" type="button" id="addRow" class="btn btn-primary" onclick="AddData" Text="Tambah data"/>
                        <asp:button runat="server" type="button" id="delRow" class="btn btn-danger" onclick="DelData" Text="Hapus data"/>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                      </div>
                    </div>
                  </div>
                </div>
            </form>
    </div>
    </div>
    <div class="KontainerBg"></div>
    <div class="Footer">
        <img src="assets/img/teamL.png" alt="Team"/>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        <img src="assets/img/user.png" alt="User"/>
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
    <script>
        function getRow(rowId) {
            document.getElementById('judul').innerHTML = "Edit Data";
            document.getElementById('formMsg').innerHTML = "Anda mengedit data dengan NPM. "+document.getElementsByClassName(rowId)[0].innerHTML;
            document.getElementById('npm').value = document.getElementsByClassName(rowId)[0].innerHTML;
            document.getElementById('nama').value = document.getElementsByClassName(rowId)[1].innerHTML;
            document.getElementById('kelas').value = document.getElementsByClassName(rowId)[2].innerHTML;
            document.getElementById('ttl').value = document.getElementsByClassName(rowId)[3].innerHTML;
            document.getElementById('alamat').value = document.getElementsByClassName(rowId)[4].innerHTML;
            requiredE();
            document.getElementById('formEdit').removeAttribute("hidden");
            document.getElementById('saveRow').removeAttribute("hidden");
            document.getElementById('addRow').setAttribute("hidden", "");
            document.getElementById('delRow').setAttribute("hidden", "");
            document.cookie = "target=" + document.getElementsByClassName(rowId)[0].innerHTML + ";";
        }

        function delRow(rowDelId) {
            rowDelId = rowDelId.substring(0, 2);
            document.getElementById('judul').innerHTML = "Delete Data";
            document.getElementById('formMsg').innerHTML = "Anda akan menghapus data dengan NPM. " +
                                                            document.getElementsByClassName(rowDelId)[0].innerHTML + ", yakin?";
            requiredD();
            document.getElementById('formEdit').setAttribute("hidden","");
            document.getElementById('delRow').removeAttribute("hidden");
            document.getElementById('addRow').setAttribute("hidden", "");
            document.getElementById('saveRow').setAttribute("hidden","");
            document.cookie = "target=" + document.getElementsByClassName(rowDelId)[0].innerHTML + ";";
        }

        function addRow() {
            document.getElementById('judul').innerHTML = "Tambah Data";
            document.getElementById('formMsg').innerHTML = "*NPM yang diinput harus bersifat unik!";
            document.getElementById('npm').value = null;
            document.getElementById('nama').value = null;
            document.getElementById('kelas').value = null;
            document.getElementById('ttl').value = null;
            document.getElementById('alamat').value = null;
            requiredE();
            document.getElementById('formEdit').removeAttribute("hidden");
            document.getElementById('addRow').removeAttribute("hidden");
            document.getElementById('delRow').setAttribute("hidden","");
            document.getElementById('saveRow').setAttribute("hidden","");
        }

        function requiredE() {
            document.getElementById('npm').setAttribute("required","");
            document.getElementById('nama').setAttribute("required","");
            document.getElementById('kelas').setAttribute("required","");
            document.getElementById('ttl').setAttribute("required","");
            document.getElementById('alamat').setAttribute("required","");
        }

        function requiredD() {
            document.getElementById('npm').removeAttribute("required");
            document.getElementById('nama').removeAttribute("required");
            document.getElementById('kelas').removeAttribute("required");
            document.getElementById('ttl').removeAttribute("required");
            document.getElementById('alamat').removeAttribute("required");
        }

    </script>
</body>
</html>
