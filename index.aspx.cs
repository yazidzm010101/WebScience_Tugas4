﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class index : System.Web.UI.Page
    {
        koneksiSQL koneksi = new koneksiSQL("localhost", "webscience", "root");
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Validasi(object sender, EventArgs e)
        {
            string userIn = Request["username"];
            string passIn = Request["password"];
            
            string querySQL = String.Format("SELECT npm FROM users WHERE npm='{0}' and password='{1}';", userIn, passIn);
            MySqlDataReader dataSQL = this.koneksi.openQuerySQL(querySQL);

            if (dataSQL.HasRows)
            {
                Response.Cookies["sessionUsr"].Value = Request["username"];
                Response.Redirect("dashboard.aspx");
            }
            else
            {
                lblModalTitle.Text = "Error";
                lblModalBody.Text = "NPM atau kata sandi tidak valid";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal('show');", true);
                upModal.Update();
            }
            dataSQL.Close();
            this.koneksi.closeQuerySQL();
        }
    }
}