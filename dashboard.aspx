﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="WebApplication1.dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Data Diri</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/css/_font.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/master.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css"/>
    <link rel="stylesheet" type="text/css" href="assets/css/myForm.css"/>
</head>
<body>
    <div class="Header">
        <div class="Home">
        <a href="dashboard.aspx">
                <img src="assets/img/teamL.png" alt="Logo"/>
            <label class="Logo">Loginin Gan</label>
        </a>
        </div>
        <div class="HeaderLink">
            <a href="index.aspx">Keluar</a>
        </div>
    </div>
    <div class="Konten">
        <form runat="server" id="Kontainer" class="Kontainer">
            <%if (GenerateKonten())
                {%>
                    <h2 id="Welcome" runat="server" class="Welcome"></h2>
                    <img class="UserPic" src="assets/img/user.png" alt="assets/img/user.png"/>
                    <label id="NPM" runat="server" class="MyID"></label>
                    <table class="table table-striped">
                        <tr class="thead-dark">
                            <th>Nama</t><td runat="server" id="namaTd"></td>
                        </tr>
                        <tr class="thead-dark">
                            <th>Kelas</th><td runat="server" id="kelasTd"></td>
                        </tr>
                        <tr class="thead-dark">
                            <th>Tempat, Tanggal Lahir</th><td runat="server" id="ttlTd"></td>
                        </tr>
                        <tr class="thead-dark">
                            <th>Alamat</th><td runat="server" id="alamatTd"></td>
                        </tr>
                    </table>
                    <br />
                    <center>
                        <asp:button cssclass="btn btn-success" runat="server" ID="btnBuka" OnClick="btnBuka_Click" Text="Buka Data"/>
                    </center>
            <%}%>
        </form>
    </div>
    <div class="KontainerBg"></div>
    <div class="Footer">
        <img src="assets/img/teamL.png" alt="Team"/>
        <div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        <img src="assets/img/user.png" alt="User"/>
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" 			    title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" 			    title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
    </div>
</body>
</html>
