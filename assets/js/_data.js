user = [
    ["56417252","Yazid123"],
    ["56417253","Zaidan123"],
    ["56417254","Mujadid123"],
    ["test","123"]
];

userDetail = {
    "56417252": [
        ["Nama" , "Yazid Zaidan Mujadid"],
        ["Tempat, Tanggal Lahir","Jakarta, 1 Januari 2001"],
        ["Alamat","Jl. Gandaria 2 Gg. Sawah 3 No. 23 16439"],
        ["Kelas","2IA01"],
        ["Jurusan","S1 Teknik Informatika"],
        ["Universitas","Universitas Gunadarma"],
    ],
    "56417253": [
        ["Nama" , "Zaidan Mujadid Yazid"],
        ["Tempat, Tanggal Lahir","Jakarta, 2 Januari 2001"],
        ["Alamat","Jl. Gandaria 2 Gg. Sawah 3 No. 23 16439"],
        ["Kelas","2IA02"],
        ["Jurusan","S1 Teknik Informatika"],
        ["Universitas","Universitas Gunadarma"],
    ],
    "56417254": [
        ["Nama" , "Mujadid Yazid Zaidan"],
        ["Tempat, Tanggal Lahir","Jakarta, 3 Januari 2001"],
        ["Alamat","Jl. Gandaria 2 Gg. Sawah 3 No. 23 16439"],
        ["Kelas","2IA03"],
        ["Jurusan","S1 Teknik Informatika"],
        ["Universitas","Universitas Gunadarma"],
    ],
    "test": [
        ["Nama" , "Testing User"],
        ["Tempat, Tanggal Lahir","Indonesia"],
        ["Alamat","Indonesia"],
        ["Kelas","2IA01"],
        ["Jurusan","Undefined"],
        ["Universitas","Undefined"],
    ],
};
